import Banner from "../components/Banner"
import Highlights from "../components/Highlights"



export default function Home() {
	return (
		<>
			<Banner
			        title="Zuitt Coding Bootcamp"
			        description="Opportunities for everyone, everywhere."
			        buttonText="Enroll now!"
			      />
      		< Highlights/>
		</>
	)
}