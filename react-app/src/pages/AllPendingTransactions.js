/*import coursesData from "../data/coursesData"*/
import {useState, useEffect,useContext} from 'react';
import UserContext from '../UserContext';
import { Button } from 'react-bootstrap';
import {Link} from "react-router-dom"
import Swal from 'sweetalert2';
import AllPendingTransactionsCard from "../components/AllPendingTransactionsCard";
import { NavLink, useNavigate } from 'react-router-dom';

export default function AllPendingTransactions() {

	const [allPendingTransactions, setAllPendingTransactions] = useState([]);
	const navigate = useNavigate("");
	const [refresh, setRefresh] = useState(false);
	const {user} = useContext(UserContext)


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/transactions/pending`, {
		  method: 'POST',
		  headers: {
		    'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('token')}`,
		  }
		})
		.then(res => res.json())
		.then(data => {
			if (data.length > 0) {
				console.log(data)
				setAllPendingTransactions(data.map(allPendingTransaction => {
				return(
					<>
					<AllPendingTransactionsCard
					        key={allPendingTransaction._id}
					        allPendingTransaction={allPendingTransaction}
					        shippedOutButton={shippedOutButton}
					        cancelOrder={cancelOrder}
					      />


					</>

				)
			}))
			} else if (data.noTransactions) {
				return Swal.fire({
            	title: 'No Pending Transactions!',
            	icon: 'warning'
          });
			} else {
				return console.log(data);
			}
			
		})
	}, [])

	useEffect(() => {
			fetch(`${process.env.REACT_APP_API_URL}/transactions/pending`, {
			  method: 'POST',
			  headers: {
			    'Content-Type': 'application/json',
			    Authorization: `Bearer ${localStorage.getItem('token')}`,
			  }
			})
			.then(res => res.json())
			.then(data => {
				if (data.length > 0) {
					setAllPendingTransactions(data.map(allPendingTransaction => {
					return(
						<>
						<AllPendingTransactionsCard
						        key={allPendingTransaction._id}
						        allPendingTransaction={allPendingTransaction}
						        shippedOutButton={shippedOutButton}
						        cancelOrder={cancelOrder}
						      />


						</>

					)
				}))
				} else if (data.noTransactions) {
				return setAllPendingTransactions([])
			} else {
				return console.log(data);
			}
			
		})
	}, [refresh])


	function shippedOutButton(transactionId) {
    fetch(`${process.env.REACT_APP_API_URL}/transactions/pendingToShip`, {
      method: 'PATCH',
      headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
              transactionId: transactionId,
            }),
          })
    .then(res => res.json())
    .then(data => {
      if(data===true) {
        Swal.fire({
                    	title: 'Order now Shipped OUt!',
                    	icon: 'success'
                  });
        setRefresh(!refresh);
        

      }

    })
  }


  function cancelOrder(transactionId) {
    fetch(`${process.env.REACT_APP_API_URL}/users/cancelTransactions`, {
      method: 'POST',
      headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
              transactionId: transactionId,
            }),
          })
    .then(res => res.json())
    .then(data => {
      if(data===true) {
        alert("cancelled success");
        setRefresh(!refresh);
        

      }

    })
  }

	return (
	  <>
	    <div style={{ display: 'flex', justifyContent: 'center', gap: '20px', marginTop: '20px' }}>
	      <NavLink to="/transactions/pending">Pending</NavLink>
	      <NavLink to="/transactions/shippedOut">Shipped Out</NavLink>
	      <NavLink to="/transactions/delivered">Delivered</NavLink>
	      <NavLink to="/transactions/cancelled">Cancelled</NavLink>
	    </div>
	    {allPendingTransactions.length > 0 ? allPendingTransactions : <p>No Pending Transactions</p>}
	  </>
	);


}

