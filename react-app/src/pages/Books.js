/*import coursesData from "../data/coursesData"*/
import {useState, useEffect} from 'react';
import BookCard from "../components/BookCard";

export default function Books() {

	const [books, setBooks] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/books/available1`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setBooks(data.map(book => {
				return(
					<BookCard key={book.id} book={book} />
				)
			}))
		})
	}, [])

	return (
		<>
		{books}
		</>
	) 
}

