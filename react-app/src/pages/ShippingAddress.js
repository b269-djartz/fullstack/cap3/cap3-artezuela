/*import coursesData from "../data/coursesData"*/
import {useState, useEffect} from 'react';
import CartCard from "../components/CartCard";
import { Button,Form } from 'react-bootstrap';
import {Link} from "react-router-dom"
import Swal from 'sweetalert2';
import ShippingAddressesCard from "../components/ShippingAddressesCard";
import { NavLink, useNavigate } from 'react-router-dom';
import locationData from '../data/locationData';

export default function ShippingAddress() {

	const [shippingAddresses, setShippingAddresses] = useState([]);
	const navigate = useNavigate("");
	const [refresh, setRefresh] = useState(false);
	const [showForm, setShowForm] = useState(false);
	const [region, setRegion] = useState('');
    const [province, setProvince] = useState('');
    const [municipality, setMunicipality] = useState('');
    const [barangay, setBarangay] = useState('');
    const [completeAddress, setCompleteAddress] = useState('');
	const [municipalityOptions, setMunicipalityOptions] = useState([]);
    const [provinceOptions, setProvinceOptions] = useState([]);
    const [barangayOptions, setBarangayOptions] = useState([]);

    const [isActive, setIsActive] = useState(false);


    const regionOptions = Object.entries(locationData).map(([regionCode, regionData]) => (
    <option key={regionCode} value={regionCode}>{regionData.region_name}</option>
    ));

     const onRegionChange = (e) => {
    const regionValue = e.target.value;
    setRegion(regionValue);
    setProvince('');
    setMunicipality('');
    setBarangay('');
  }

     useEffect(() => {
    if (region === '') {
      setProvinceOptions([]);
      setMunicipalityOptions([]);
      setBarangayOptions([]);
      setIsActive(false);
    } else {
      const provinces = Object.entries(locationData[region].province_list).map(([provinceName, provinceData]) => (
        <option key={provinceName} value={provinceName}>{provinceName}</option>
      ));
      setProvinceOptions(provinces);
      setMunicipalityOptions([]);
      setBarangayOptions([]);
      setIsActive(false);
    }
  }, [region]);

    const onProvinceChange = (e) => {
    const provinceValue = e.target.value;
    setProvince(provinceValue);
    setMunicipality('');
    setBarangay('');
  }

    useEffect(() => {
    if (province === '') {
      setMunicipalityOptions([]);
      setBarangayOptions([]);
      setIsActive(false);
    } else {
      const municipalities = Object.entries(locationData[region].province_list[province].municipality_list).map(([municipalityName, municipalityData]) => (
        <option key={municipalityName} value={municipalityName}>{municipalityName}</option>
      ));
      setMunicipalityOptions(municipalities);
      setBarangayOptions([]);
      setIsActive(false);
    }
  }, [province]);

    const onMunicipalityChange = (e) => {
    const municipalityValue = e.target.value;
    setMunicipality(municipalityValue);
    setBarangay('');
  }

    useEffect(() => {
    if (municipality === '') {
      setBarangayOptions([]);
      setIsActive(false);
    } else {
      const barangays = locationData[region].province_list[province].municipality_list[municipality].barangay_list.map((barangayName) => (
        <option key={barangayName} value={barangayName}>{barangayName}</option>
      ));
      setBarangayOptions(barangays);
      setIsActive(false);
    }
  }, [municipality]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/getUserShippingAddress`, {
		  method: 'POST',
		  headers: {
		    'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('token')}`,
		  }
		})
		.then(res => res.json())
		.then(data => {
			if (data.length > 0) {
				setShippingAddresses(data.map(shippingAddress => {
				return(
					<>
					<ShippingAddressesCard
					        key={shippingAddress._id}
					        shippingAddress={shippingAddress}
					        addShippingAddress={addShippingAddress}
					        chooseShippingAddress={chooseShippingAddress}
					      />


					</>

				)
			}))
			} else {
				return console.log(data);
			}
			
		})
	}, [])

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/getUserShippingAddress`, {
		  method: 'POST',
		  headers: {
		    'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('token')}`,
		  }
		})
		.then(res => res.json())
		.then(data => {
			if (data.length > 0) {
				setShippingAddresses(data.map(shippingAddress => {
				return(
					<>
					<ShippingAddressesCard
					        key={shippingAddress._id}
					        shippingAddress={shippingAddress}
					        chooseShippingAddress={chooseShippingAddress}
					      />


					</>

				)
			}))
			} else {
				return console.log(data);
			}
			
		})
	}, [refresh])


	function chooseShippingAddress(shippingAddressId) {
    fetch(`${process.env.REACT_APP_API_URL}/users/chooseShippingAddress`, {
      method: 'POST',
      headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
              shippingAddressId: shippingAddressId,
            }),
          })
    .then(res => res.json())
    .then(data => {
    	console.log(data)
    	console.log(shippingAddressId)
      if(data===true) {
      	setRefresh(!refresh)
      	console.log(data)
        

      } else {
      	console.log(data)
      }

    })
  }


	function addShippingAddress() {
    fetch(`${process.env.REACT_APP_API_URL}/users/addShippingAddress`, {
      method: 'POST',
      headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
              region: region,
              province: province,
              municipality: municipality,
              barangay: barangay,
              completeAddress: completeAddress

            }),
          })
    .then(res => res.json())
    .then(data => {
      if(data===true) {
        alert("shipping added");
        setRefresh(!refresh);
        setShowForm(false)
        

      }

    })
  }


  



const handleUseAddress = () => {
  	setRefresh(!refresh)
  }



  const handleShowForm = (e) => {
  	e.preventDefault()
  	setShowForm(true)
  }

  const handleCancel = (e) => {
  	e.preventDefault()
  	setShowForm(false)
  }


	return (
	  <>
	  {showForm ? 
	  
	  (

	  <>
	  <Form.Group controlId="region">
          <Form.Label>Region</Form.Label>
          <Form.Control as="select" value={region} onChange={(e) => setRegion(e.target.value)} required>
            <option value="">-- Select Region --</option>
            {regionOptions}
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="province">
          <Form.Label>Province</Form.Label>
          <Form.Control as="select" value={province} onChange={(e) => setProvince(e.target.value)} required>
            <option value="">-- Select Province --</option>
            {provinceOptions}
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="municipality">
          <Form.Label>Municipality</Form.Label>
          <Form.Control as="select" value={municipality} onChange={(e) => setMunicipality(e.target.value)} required>
            <option value="">-- Select Municipality --</option>
            {municipalityOptions}
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="barangay">
          <Form.Label>Barangay</Form.Label>
          <Form.Control as="select" value={barangay} onChange={(e) => setBarangay(e.target.value)} required>
            <option value="">-- Select Barangay --</option>
            {barangayOptions}
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="completeAddress">
        <Form.Label>Complete Address</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter street,apartment or house no. "
          value={completeAddress}
          onChange={(e) => setCompleteAddress(e.target.value)}
          required
         />
         </Form.Group>
         <Button
	  	  className="bg-primary"
	  	  onClick={addShippingAddress}
	  			>
	  	  Save
	  		</Button>
	  		<Button
	  	  className="bg-secondary"
	  	  onClick={handleCancel}
	  	>
	  	  Cancel
	  	</Button>
       </>


         
         )


	  : 

	  (
 			<>
 			<Button
	  	  className="bg-primary"
	  	  onClick={handleShowForm}
	  	>
	  	  Add new Shipping Address
	  	</Button>

	  	<Button
  className="bg-success"
  as={Link}
  to="/orders/checkout"
  onClick={() => {
    // Handle click event here
    handleUseAddress();
  }}
>
  Use Address
</Button>
	  	</>

	  	)
	 

	}
	  	
	    {shippingAddresses.length > 0 ? shippingAddresses : <p>No shipping Address</p>}
	  </>
	);


}

