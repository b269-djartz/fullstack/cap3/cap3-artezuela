import { useState, useContext } from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function ShippingAddressesCard({ shippingAddress, chooseShippingAddress }) {
  const { user } = useContext(UserContext);

  const {
    region,
    province,
    municipality,
    barangay,
    completeAddress,
    _id,
  } = shippingAddress;



  return (
    <>
      <Row className="mt-3 mb-3">
        <Col xs={12}>
          <Card className="cardHighlight p-0">
            <div className="d-flex align-items-center justify-content-end p-2">
              <input type="radio" name="shippingAddress"  onChange={() => chooseShippingAddress(shippingAddress._id)} style={{width: '24px', height: '24px'}} />
            </div>
            <Card.Body>
              <Card.Subtitle>Region</Card.Subtitle>
              <Card.Text>{region}</Card.Text>
              <Card.Subtitle>Province</Card.Subtitle>
              <Card.Text>{province}</Card.Text>
              <Card.Subtitle>Municipality</Card.Subtitle>
              <Card.Text>{municipality}</Card.Text>
              <Card.Subtitle>Barangay</Card.Subtitle>
              <Card.Text>{barangay}</Card.Text>
              <Card.Subtitle>CompleteAddress</Card.Subtitle>
              <Card.Text>{completeAddress}</Card.Text>
              <Card.Subtitle>Voucher Discount</Card.Subtitle>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </>
  );
}
